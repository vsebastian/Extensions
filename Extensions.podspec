#
# Be sure to run `pod lib lint Extensions.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Extensions'
  s.version          = '0.1.9'
  s.summary          = 'Most used extensions.'
  s.description      = 'Most used extensions.'

  s.homepage         = 'https://gitlab.com/popaaaandrei/Extensions'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'popaaaandrei' => 'popaaaandrei@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/popaaaandrei/Extensions.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/popaaaandrei'

  s.ios.deployment_target = '10.0'
  s.swift_version = '5'
  s.source_files = 'Extensions/Classes/**/*'

end
