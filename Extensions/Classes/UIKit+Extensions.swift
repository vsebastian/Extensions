//
//  Extensions.swift
//
//
//  Created by andrei on 25/11/2017.
//  Copyright © 2016 andrei. All rights reserved.
//

import UIKit




// ============================================================================
// MARK: UIFont
// ============================================================================
public extension UIFont {
    
    /// display all installed fonts
    class func displayAllFonts() {
        for family in UIFont.familyNames {
            print("family: \(family)")
            for names in UIFont.fontNames(forFamilyName: family) {
                print("== \(names)")
            }
        }
    }
    
}

// ============================================================================
// MARK: UILabel
// ============================================================================
public extension UILabel {
    
    func set(text: String?, lineSpacing: CGFloat) {
        guard let text = text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        // paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString: NSMutableAttributedString
        if let labelAttributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelAttributedText)
        } else {
            attributedString = NSMutableAttributedString(string: text)
        }
        
        // line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                      value: paragraphStyle,
                                      range: NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
    
    
    func set(text: String?,
             lineSpacing: CGFloat,
             keywords: [String],
             keywordFont: UIFont,
             keywordColor: UIColor) {
        
        guard let text = text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        
        let attributedString: NSMutableAttributedString
        if let labelAttributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelAttributedText)
        } else {
            attributedString = NSMutableAttributedString(string: text)
        }
        
        // line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                      value: paragraphStyle,
                                      range: NSMakeRange(0, attributedString.length))
        
        for keyword in keywords {
            let range = (text as NSString).range(of: keyword)
            
            // if keyword present, highlight
            if range.location != NSNotFound {
                attributedString.addAttribute(NSAttributedString.Key.font,
                                              value: keywordFont,
                                              range: range)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: keywordColor,
                                              range: range)
            }
        }
        
        self.attributedText = attributedString
    }
}



// ============================================================================
// MARK: UIColor
// ============================================================================
public extension UIColor {
    
    ///
    convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    
    /**
     Creates an immuatble UIColor instance specified by a hex string, CSS color name, or nil.
     - parameter hexString: A case insensitive String? representing a hex or CSS value e.g.
     - **"abc"**
     - **"abc7"**
     - **"#abc7"**
     - **"00FFFF"**
     - **"#00FFFF"**
     - **"00FFFF77"**
     - **nil** [UIColor clearColor]
     - **empty string** [UIColor clearColor]
     */
    convenience init(hex: String?) {
        let normalizedHexString: String = UIColor.normalize(hex)
        var c: CUnsignedInt = 0
        Scanner(string: normalizedHexString).scanHexInt32(&c)
        self.init(red:UIColorMasks.redValue(c), green:UIColorMasks.greenValue(c), blue:UIColorMasks.blueValue(c), alpha:UIColorMasks.alphaValue(c))
    }
    
    private enum UIColorMasks: CUnsignedInt {
        case redMask    = 0xff000000
        case greenMask  = 0x00ff0000
        case blueMask   = 0x0000ff00
        case alphaMask  = 0x000000ff
        
        static func redValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat((value & redMask.rawValue) >> 24) / 255.0
        }
        
        static func greenValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat((value & greenMask.rawValue) >> 16) / 255.0
        }
        
        static func blueValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat((value & blueMask.rawValue) >> 8) / 255.0
        }
        
        static func alphaValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat(value & alphaMask.rawValue) / 255.0
        }
    }
    
    private static func normalize(_ hex: String?) -> String {
        guard var hexString = hex else {
            return "00000000"
        }
        if hexString.hasPrefix("#") {
            hexString = String(hexString.dropFirst())
        }
        if hexString.count == 3 || hexString.count == 4 {
            hexString = hexString.map { "\($0)\($0)" } .joined()
        }
        let hasAlpha = hexString.count > 7
        if !hasAlpha {
            hexString += "ff"
        }
        return hexString
    }
    
    
    /**
     Returns a hex equivalent of this UIColor.
     - Parameter includeAlpha:   Optional parameter to include the alpha hex.
     color.hexDescription() -> "ff0000"
     color.hexDescription(true) -> "ff0000aa"
     - Returns: A new string with `String` with the color's hexidecimal value.
     */
    func hexDescription(_ includeAlpha: Bool = false) -> String {
        guard self.cgColor.numberOfComponents == 4 else {
            return "Color not RGB."
        }
        let a = self.cgColor.components!.map { Int($0 * CGFloat(255)) }
        let color = String.init(format: "%02x%02x%02x", a[0], a[1], a[2])
        if includeAlpha {
            let alpha = String.init(format: "%02x", a[3])
            return "\(color)\(alpha)"
        }
        return color
    }
    
    
    /// make color darker by factor
    func darker(factor: CGFloat) -> UIColor {
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if self.getRed(&r, green: &g, blue: &b, alpha: &a) {
            return UIColor(
                red: max(r - factor, 0.0),
                green: max(g - factor, 0.0),
                blue: max(b - factor, 0.0),
                alpha: a)
        }
        
        return UIColor()
    }
    
    /// make color lighter by factor
    func lighter(factor: CGFloat) -> UIColor {
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if self.getRed(&r, green: &g, blue: &b, alpha: &a) {
            return UIColor(
                red: min(r + factor, 1.0),
                green: min(g + factor, 1.0),
                blue: min(b + factor, 1.0),
                alpha: a)
        }
        
        return UIColor()
    }
    
    
    func image() -> UIImage? {
        return UIImage.from(color: self)
    }
    
}


// ============================================================================
// MARK: UIBezierPath
// ============================================================================
public extension UIBezierPath {

    func image(ofSize size: CGSize, fillColor color: UIColor) -> UIImage? {
        
        guard size.width > 0 && size.height > 0 else {
            return nil
        }
        
        // create the graphics context
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.saveGState()
        
        //// Bezier Drawing
        color.setFill()
        lineCapStyle = CGLineCap.round
        fill()
        
        context.restoreGState()
        // save the image from the implicit context into an image
        let result = UIGraphicsGetImageFromCurrentImageContext()
        // cleanup
        UIGraphicsEndImageContext()
        
        return result
    }
    
    func image(ofSize size: CGSize, strokeColor color: UIColor) -> UIImage? {
        
        guard size.width > 0 && size.height > 0 else {
            return nil
        }
        
        // create the graphics context
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        context.saveGState()
        
        //// Bezier Drawing
        color.setStroke()
        lineCapStyle = CGLineCap.round
        stroke()
        
        context.restoreGState()
        // save the image from the implicit context into an image
        let result = UIGraphicsGetImageFromCurrentImageContext()
        // cleanup
        UIGraphicsEndImageContext()
        
        return result
    }

    func image(ofSize size: CGSize, gradient: CGGradient, angle: CGFloat = -45) -> UIImage? {
        
        guard size.width > 0 && size.height > 0 else {
            return nil
        }
        
        // create the graphics context
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.saveGState()
        
        addClip()
        let bezierRotatedPath = UIBezierPath()
        bezierRotatedPath.append(self)
        var bezierTransform = CGAffineTransform(rotationAngle: angle * -CGFloat.pi/180)
        bezierRotatedPath.apply(bezierTransform)
        let bezierBounds = bezierRotatedPath.cgPath.boundingBoxOfPath
        bezierTransform = bezierTransform.inverted()
        
        
        context.drawLinearGradient(gradient,
                                   start: CGPoint(x: bezierBounds.minX, y: bezierBounds.midY).applying(bezierTransform),
                                   end: CGPoint(x: bezierBounds.maxX, y: bezierBounds.midY).applying(bezierTransform),
                                   options: [])
        context.restoreGState()
        
        
        // save the image from the implicit context into an image
        let result = UIGraphicsGetImageFromCurrentImageContext()
        // cleanup
        UIGraphicsEndImageContext()
        
        return result
    }
}


// ============================================================================
// MARK: UIImage
// ============================================================================
public extension UIImage {
    

    class func from(color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)

        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        color.setFill()
        context.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    class func from(color: UIColor, size: CGSize) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 5.0)

        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        path.fill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    class func from(file: String, type: String = "jpg") -> UIImage? {
        guard let path = Bundle.main.path(forResource: file, ofType: type) else { return nil }
        return UIImage(contentsOfFile: path)
    }
    
    
    func resize(size: CGSize) -> UIImage? {
        let scale     = UIScreen.main.scale
        let size      = scale > 1 ? CGSize(width: size.width/scale, height: size.height/scale) : size
        let imageRect = CGRect(x: 0, y: 0, width: size.width, height: size.width)
        
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        self.draw(in: imageRect)
        let scaled = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaled
    }
    
    func resizeAspectFit(size: CGSize) -> UIImage? {
        let aspectFitSize = self.getAspectFitRect(origin: self.size, destination: size)
        let resizedImage = self.resize(size: aspectFitSize)
        return resizedImage
    }
    
    func getAspectFitRect(origin src: CGSize, destination dst: CGSize) -> CGSize {
        var result = CGSize.zero
        var scaleRatio = CGPoint()
        
        if (dst.width != 0) {scaleRatio.x = src.width / dst.width}
        if (dst.height != 0) {scaleRatio.y = src.height / dst.height}
        let scaleFactor = max(scaleRatio.x, scaleRatio.y)
        
        result.width  = scaleRatio.x * dst.width / scaleFactor
        result.height = scaleRatio.y * dst.height / scaleFactor
        return result
    }
    
    func tint(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        
        // flip the image
        context.scaleBy(x: 1.0, y: -1.0)
        context.translateBy(x: 0.0, y: -self.size.height)
        
        // multiply blend mode
        context.setBlendMode(.multiply)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context.fill(rect)
        
        // create UIImage
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        UIGraphicsEndImageContext()
        return newImage
    }
}



// ============================================================================
// MARK: UIView
// ============================================================================
public extension UIView {
    
    // remember to add gradientLayer.frame = self.view.bounds
    // in viewDidLayoutSubviews() / layoutSubviews()
    func applyGradient(gradientLayer: CAGradientLayer, colors: [UIColor], startPoint: CGPoint = CGPoint(x: 0.0, y: 0.0), endPoint: CGPoint = CGPoint(x: 0.0, y: 1.0)) {
        
        guard colors.count > 1 else {
            return
        }
        
        gradientLayer.frame = self.bounds
        gradientLayer.colors = colors.map { $0.cgColor }
        
        let locations = [Int](0..<colors.count)
            .map { Double($0) / Double(colors.count-1) }
        
        gradientLayer.locations = locations.map { NSNumber(value: $0) }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        gradientLayer.removeFromSuperlayer()
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    func applyGradient(colors: [UIColor], startPoint: CGPoint = CGPoint(x: 0.0, y: 0.0), endPoint: CGPoint = CGPoint(x: 0.0, y: 1.0)) {
        
        guard colors.count > 1 else {
            return
        }
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = colors.map { $0.cgColor }
        
        let locations = [Int](0..<colors.count)
            .map { Double($0) / Double(colors.count-1) }
        
        gradientLayer.locations = locations.map { NSNumber(value: $0) }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        self.layer.addSublayer(gradientLayer)
    }
    
}



// ============================================================================
// MARK: UITextField
// ============================================================================
public extension UITextField {
    
    func setupTheme(
        borderColor: UIColor? = nil,
        borderWidth: CGFloat? = nil,
        backgroundColor: UIColor? = .white,
        cornerRadius: CGFloat,
        textColor: UIColor,
        textFont: UIFont? = nil,
        placeholderText: String? = nil,
        placeholderColor: UIColor? = nil,
        placeholderFont: UIFont? = nil
        ) {
        
        self.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        
        if let borderWidth = borderWidth, let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = borderWidth
        }
        self.layer.cornerRadius = cornerRadius
        
        if let backgroundColor = backgroundColor {
            self.layer.backgroundColor = backgroundColor.cgColor
        }
        
        if let textFont = textFont {
            self.font = textFont
        }
        
        self.textColor = textColor
        
        if let placeholderText = placeholderText,
            let placeholderColor = placeholderColor,
            let placeholderFont = placeholderFont {
            
            self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor, NSAttributedString.Key.font: placeholderFont])
        }
    }

    
    func setupAccessoryRightButton(title: String, titleColor: UIColor, titleFont: UIFont?, block: @escaping (_ sender: UIButton) -> Void) {
        
        let availableWidth = self.frame.size.width / 2
        let availableHeight = self.frame.size.height * 0.8
        
        
        var titleSize = CGSize.zero
        if let titleFont = titleFont {
            titleSize = title.size(constrainedToWidth: availableWidth, height: availableHeight, font: titleFont)
        }
        else {
            titleSize = title.size(constrainedToWidth: availableWidth, height: availableHeight, font: self.font!)
        }
        
        let Δ = self.layer.cornerRadius > 0 ? 10 + self.layer.cornerRadius / 2 : 20
        
        let buttonWidth = titleSize.width + Δ
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: buttonWidth, height: availableHeight))
        button.setTitle(title, for: UIControl.State.normal)
        button.setTitleColor(textColor, for: UIControl.State.normal)
        button.setTitleColor(textColor?.withAlphaComponent(0.5), for: UIControl.State.highlighted)
        button.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addAction(block: block)
        
        if let titleFont = titleFont {
            button.titleLabel?.font = titleFont
        }
        else {
            button.titleLabel?.font = self.font
        }
        
        self.rightView = button
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    func setupAccessoryLeftView(imageName: String, scaleFactor: CGFloat) {
        let availableHeight = self.frame.size.height * scaleFactor
        
        let leftView = UIImageView(frame: CGRect(x: 0, y: 0, width: availableHeight, height: availableHeight))
        
        leftView.contentMode = UIView.ContentMode.scaleAspectFit
        leftView.image = UIImage(named: imageName)
        
        self.leftView = leftView;
        self.leftViewMode = UITextField.ViewMode.always
    }
    
    func setupAccessoryRightButton(imageName: String, scaleFactor: CGFloat?, target: AnyObject?, action: Selector) {
        
        guard let buttonImage = UIImage(named: imageName) else {
            return
        }
        
        var availableHeight : CGFloat = 22
        if let scaleFactor = scaleFactor {
            availableHeight = self.frame.size.height * scaleFactor
        }
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: availableHeight, height: availableHeight))
        
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        button.contentMode = UIView.ContentMode.scaleAspectFit
        button.clipsToBounds = true
        button.setImage(buttonImage, for: UIControl.State.normal)
        button.addTarget(target, action: action, for: .touchUpInside)
        
        self.rightView = button
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    func rightButton() -> UIButton? {
        if let rightView = rightView , rightView is UIButton {
            return rightView as? UIButton
        }
        
        return nil
    }
    
}


// ============================================================================
// MARK: UIButton
// ============================================================================
var ButtonAssociatedObjectHandle: UInt8 = 0

class ClosureWrapper : NSObject {
    var block : (_ sender: UIButton) -> Void
    init(block: @escaping (_ sender: UIButton) -> Void) {
        self.block = block
    }
}


public extension UIButton {
    
    func addAction(forControlEvents events: UIControl.Event = UIControl.Event.touchUpInside, block: @escaping (_ sender: UIButton) -> Void) {
        let wrapper = ClosureWrapper(block: block)
        objc_setAssociatedObject(self, &ButtonAssociatedObjectHandle, wrapper, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        addTarget(self, action: #selector(UIButton.block_handleAction), for: events)
    }
    
    @objc func block_handleAction(sender: UIButton) {
        let wrapper = objc_getAssociatedObject(self, &ButtonAssociatedObjectHandle) as! ClosureWrapper
        wrapper.block(sender)
    }
    
    func setupTheme(tintColor: UIColor? = nil,
                    borderColor: UIColor = UIColor.clear,
                    borderWidth: CGFloat = 0,
                    backgroundColorNormal: UIColor? = nil,
                    backgroundColorHighlighted: UIColor? = nil,
                    cornerRadius: CGFloat = 0,
                    titleColor: UIColor? = nil,
                    titleColorHighlighted: UIColor? = nil,
                    title: String? = nil,
                    titleFont: UIFont? = nil
        ) {
        
        self.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        self.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        
        if let tintColor = tintColor {
            self.tintColor = tintColor
        }
        
        if let titleFont = titleFont {
            self.titleLabel?.font = titleFont
        }
        
        if let title = title {
            self.setTitle(title, for: UIControl.State.normal)
            self.setTitle(title, for: UIControl.State.highlighted)
        }
        
        if let titleColor = titleColor {
            self.setTitleColor(titleColor, for: UIControl.State.normal)
        }
        
        if let titleColorHighlighted = titleColorHighlighted {
            self.setTitleColor(titleColorHighlighted, for: UIControl.State.highlighted)
        }
        
        if let backgroundColorNormal = backgroundColorNormal {
            self.setBackgroundImage(UIImage.from(color: backgroundColorNormal), for: UIControl.State.normal)
        }
        
        if let backgroundColorHighlighted = backgroundColorHighlighted {
            self.setBackgroundImage(UIImage.from(color: backgroundColorHighlighted), for: UIControl.State.highlighted)
        }
    }
    
    
    func setupImage(image: UIImage) {
        self.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        self.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        self.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        self.contentMode = UIView.ContentMode.scaleAspectFit
        // self.layer.masksToBounds = true
        self.clipsToBounds = true
        // self.layer.borderColor = UIColor.greenColor().CGColor
        // self.layer.borderWidth = 1
        self.setImage(image, for: UIControl.State.normal)
        self.setImage(image, for: UIControl.State.highlighted)
    }
    
}


// ============================================================================
// MARK: UIBarButtonItem
// ============================================================================
public extension UIBarButtonItem {
    
    convenience init?(withSize: CGSize = CGSize(width: 24, height: 24),
                      configureButton: ((_ button: UIButton) -> Void)? = nil,
                      block: @escaping (_ sender: UIButton) -> Void) {
        
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: withSize))
        button.contentMode = UIView.ContentMode.scaleAspectFit
        button.clipsToBounds = true
        button.addAction(block: block)
        configureButton?(button)
        
        if #available(iOS 11.0, *) {
            button.widthAnchor.constraint(equalToConstant: withSize.width).isActive = true
            button.heightAnchor.constraint(equalToConstant: withSize.height).isActive = true
        }
        
        self.init(customView: button)
    }
    
    
    convenience init?(imageName: String,
                      ofSize: CGSize = CGSize(width: 24, height: 24),
                      block: @escaping (_ sender: UIButton) -> Void) {
        guard let image = UIImage(named: imageName) else { return nil }
        self.init(image: image, ofSize: ofSize, block: block)
    }
    
    
    convenience init?(image: UIImage? = nil, ofSize: CGSize = CGSize(width: 24, height: 24), block: @escaping (_ sender: UIButton) -> Void) {
        
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: ofSize))
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        button.contentMode = UIView.ContentMode.scaleAspectFit
        button.clipsToBounds = true
        button.setImage(image, for: UIControl.State.normal)
        button.addAction(block: block)
        
        if #available(iOS 11.0, *) {
            button.widthAnchor.constraint(equalToConstant: ofSize.width).isActive = true
            button.heightAnchor.constraint(equalToConstant: ofSize.height).isActive = true
        }
        
        self.init(customView: button)
    }
    
    
    convenience init?(imageName: String,
                      target: AnyObject?,
                      action: Selector) {
        
        guard let buttonImage = UIImage(named: imageName) else {
            return nil
        }
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        button.contentMode = UIView.ContentMode.scaleAspectFit
        button.clipsToBounds = true
        button.setImage(buttonImage, for: UIControl.State.normal)
        button.addTarget(target, action: action, for: .touchUpInside)
        
        self.init(customView: button)
    }
    
    func setup(image: UIImage, forState: UIControl.State) {
        if let button = self.customView as? UIButton {
            button.setImage(image, for: forState)
        }
    }
    
}




// ============================================================================
// MARK: CALayer
// ============================================================================
public extension CALayer {
    
    func addShadow(withOffset: CGSize,
                   radius: CGFloat,
                   color: UIColor,
                   opacity: Float,
                   rasterize: Bool = true) {
        
        masksToBounds = false
        shadowColor = color.cgColor
        shadowOpacity = opacity
        shadowOffset = withOffset
        shadowRadius = radius
        shouldRasterize = rasterize
        rasterizationScale = UIScreen.main.scale
    }
}


// ============================================================================
// MARK: UIGestureRecognizer
// Created by Hsoi, Swift-ized by JLandon.
// ============================================================================

public extension UIGestureRecognizer {
    
    private class GestureAction {
        var action: (UIGestureRecognizer) -> Void
        
        init(action: @escaping (UIGestureRecognizer) -> Void) {
            self.action = action
        }
    }
    
    private struct AssociatedKeys {
        static var ActionName = "action"
    }
    
    private var gestureAction: GestureAction? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionName, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionName) as? GestureAction }
    }
    
    
    convenience init(action: @escaping (UIGestureRecognizer) -> Void) {
        self.init()
        gestureAction = GestureAction(action: action)
        addTarget(self, action: #selector(UIGestureRecognizer.handleAction))
    }
    
    @objc dynamic private func handleAction(recognizer: UIGestureRecognizer) {
        gestureAction?.action(recognizer)
    }
    
}



// ============================================================================
// MARK: UINavigationBar
// ============================================================================
public extension UINavigationBar {
    
    class func setNavigationBar(barStyle: UIBarStyle = UIBarStyle.default,
                                translucent: Bool = true,
                                tintColor: UIColor,
                                bgColor: UIColor,
                                font: UIFont,
                                fontButtons: UIFont) {
        
        // navigation bar
        UINavigationBar.appearance().barStyle = barStyle
        UINavigationBar.appearance().barTintColor = bgColor
        UINavigationBar.appearance().tintColor = tintColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: font,
                                                            NSAttributedString.Key.foregroundColor: tintColor]
        // bar buttons
        UIBarButtonItem.appearance().tintColor = tintColor
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: fontButtons],
                                                            for: UIControl.State.normal)
    }
    
    class func removeNavigationBarShadow() {
        // remove bottom shadow line
        UINavigationBar.appearance().shadowImage = UIImage()
        // UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    }
    
    class func setupTransparentLight() {
        UINavigationBar.removeNavigationBarShadow()
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().isTranslucent = true
    }
    
}





// ============================================================================
// MARK: UITabBar
// ============================================================================
public extension UITabBar {
    
    class func setupTabBarColor(color: UIColor? = nil,
                                lineVisible : Bool = false,
                                lineColor: UIColor = UIColor.clear) {
        if let color = color {
            UITabBar.appearance().backgroundImage = UIImage.from(color: color)
            UITabBar.appearance().barTintColor = color
        } else {
            UITabBar.appearance().backgroundImage = UIImage()
            UITabBar.appearance().barTintColor = UIColor.clear
        }
        
        if lineVisible == true {
            UITabBar.appearance().shadowImage = UIImage.from(color: lineColor)
        } else {
            UITabBar.appearance().shadowImage = UIImage()
        }
    }
}


// ============================================================================
// MARK: UITableView & Cell
// ============================================================================
public extension UITableView {
    /// Returns: True if cell at section and row is visible, False otherwise
    func isCellVisible(section: Int, row: Int) -> Bool {
        guard let indexes = self.indexPathsForVisibleRows else { return false }
        return indexes.contains {$0.section == section && $0.row == row }
    }
}

public extension UITableViewCell {
    static var identifier: String {
        return NSStringFromClass(self.classForCoder()).components(separatedBy: ".").last!
    }
}



// ============================================================================
// MARK: UICollectionView & Cell
// ============================================================================
public extension UICollectionViewCell {
    static var identifier: String {
        return NSStringFromClass(self.classForCoder()).components(separatedBy: ".").last!
    }
}
