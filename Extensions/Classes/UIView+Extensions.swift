//
//  UIView+Extensions.swift
//
//
//  Created by andrei on 25/11/2017.
//  Copyright © 2016 andrei. All rights reserved.
//

import UIKit





@available(iOS 9.0, *)
public extension UIView {
    
    @discardableResult
    func pin(to: UIView,
             insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0),
             safeArea: Bool = false) -> [NSLayoutConstraint] {

        var constraints = [NSLayoutConstraint]()

        if #available(iOS 11.0, *), safeArea {
            constraints.append(topAnchor.constraint(equalTo: to.safeAreaLayoutGuide.topAnchor, constant: insets.top))
        } else {
            constraints.append(topAnchor.constraint(equalTo: to.topAnchor, constant: insets.top))
        }

        constraints.append(leadingAnchor.constraint(equalTo: to.leadingAnchor,
                                                    constant: insets.left))
        constraints.append(trailingAnchor.constraint(equalTo: to.trailingAnchor,
                                                     constant: -insets.right))
        constraints.append(bottomAnchor.constraint(equalTo: to.bottomAnchor,
                                                   constant: -insets.bottom))

        NSLayoutConstraint.activate(constraints)

        return constraints
    }
    
    
    func add(to view: UIView, safeArea: Bool = false) {
        translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self)
        pin(to: view, safeArea: safeArea)
    }
    
    func add(to view: UIView, insets: UIEdgeInsets, safeArea: Bool = false) {
        translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self)
        pin(to: view, insets: insets, safeArea: safeArea)
    }
    
    func addBorder(opacity: CGFloat = 0.2, w: CGFloat = 1, color: UIColor = .gray) {
        layer.borderColor = color.withAlphaComponent(opacity).cgColor
        layer.borderWidth = w
    }
    
    
    func roundMask(corners: UIRectCorner = [.topLeft, .topRight],
                   radius: CGFloat = 10,
                   rect: CGRect? = nil) {
        
        guard let rect = rect else {
            let maskPath = UIBezierPath(roundedRect: bounds,
                                        byRoundingCorners: corners,
                                        cornerRadii: CGSize(width: radius, height: radius))
            let shape = CAShapeLayer()
            shape.path = maskPath.cgPath
            layer.mask = shape
            return
        }
        
        let maskPath = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
    
    
    /// add parallax effect
    func addParallax(amount: CGFloat = 20) {
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        // let horizontal = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .TiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        // let vertical = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .TiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        self.addMotionEffect(group)
    }
    

    
    
    struct Constants {
        static let ExternalBorderName = "externalBorder"
    }
    
    func addExternalBorder(width borderWidth: CGFloat = 1,
                           color borderColor: UIColor,
                           cornerRadius: CGFloat = 5) {
        
        let externalBorder = CALayer()
        externalBorder.frame = CGRect(x: -borderWidth * 2,
                                      y: -borderWidth * 2,
                                      width: frame.size.width + 6 * borderWidth,
                                      height: frame.size.height + 6 * borderWidth)
        
        externalBorder.borderColor = borderColor.cgColor
        externalBorder.borderWidth = borderWidth
        externalBorder.cornerRadius = cornerRadius
        externalBorder.name = Constants.ExternalBorderName
        layer.insertSublayer(externalBorder, at: 0)
        layer.masksToBounds = false
    }
    
    
    func removeExternalBorders() {
        layer.sublayers?.filter() { $0.name == Constants.ExternalBorderName }.forEach() {
            $0.removeFromSuperlayer()
        }
    }
    
    func removeExternalBorder(externalBorder: CALayer) {
        guard externalBorder.name == Constants.ExternalBorderName else { return }
        externalBorder.removeFromSuperlayer()
    }
    
}
